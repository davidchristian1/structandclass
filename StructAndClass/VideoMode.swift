//
//  VideoMode.swift
//  StructAndClass
//
//  Created by David Christian on 28/04/20.
//  Copyright © 2020 Apple Academy. All rights reserved.
//

import Foundation

/**
 Ini adalah class untuk Video Mode yang berisi nama, interlaced, frame rate dan resolusi. Video Mode ini berkaitan dengan Resolution.
 */
class VideoMode{
    var resolution = Resolution()
    var interlaced = false
    var frameRate = 0.0
    var name:String?
}
