//
//  Resolution.swift
//  StructAndClass
//
//  Created by David Christian on 28/04/20.
//  Copyright © 2020 Apple Academy. All rights reserved.
//

import Foundation

/**
 Ini adalah struct untuk Resolution
 */
struct Resolution{
    var width = 0
    var height = 0
}
