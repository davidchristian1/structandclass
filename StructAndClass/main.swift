//
//  main.swift
//  StructAndClass
//
//  Created by David Christian on 28/04/20.
//  Copyright © 2020 Apple Academy. All rights reserved.
//

import Foundation

let someResolution = Resolution()
let someVideoMode = VideoMode()

print("the width of some resolution is \(someResolution.width)")
print("the width of some video mode is \(someVideoMode.resolution.width)")

// bisa mengganti value dengan dot syntax juga
someVideoMode.resolution.width = 1280
print("the width of some video mode is now \(someVideoMode.resolution.width)")

let vga = Resolution(width: 640, height: 480)

// intinya disini hd value nya dicopy ke cinema jadi begitu cinema valuenya diganti, value hd tidak ikut keganti karena Resolution itu pake struct
// Struct sudah ada initializer sendiri sedangkan class harus buat lagi initnya
let hd = Resolution(width: 1920, height: 1080)
var cinema = hd

cinema.width = 2048

print("HD is still \(hd.width)")
print("cinema is now \(cinema.width)")

// Sama juga dengan enum
var currentDirection = CompassPoint.west
let rememberedDirection = currentDirection
currentDirection.turnNorth()

print("The Current Direction is \(currentDirection)")
print("The Remembered Direction is \(rememberedDirection)")

// Kalo class dia reference type, jadi ketika ada alsoTenEighty yang disamakan dengan tenEighty nanti value dari alsoTenEighty akan mereferensikan value tenEighty. jadi ketika alsoTenEighty diganti, maka tenEighty nya juga ikut keganti.
let tenEighty = VideoMode()
tenEighty.frameRate = 25.0
tenEighty.name = "1080i"
tenEighty.interlaced = true
tenEighty.resolution = hd

let alsoTenEighty = tenEighty
alsoTenEighty.frameRate = 30.0

print("The framerate for ten eighty is now \(tenEighty.frameRate)")


if tenEighty === alsoTenEighty{
    print("tenEighty dan alsoTenEighty mereferensikan pada VideoMode instance yang sama")
}
