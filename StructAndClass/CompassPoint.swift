//
//  CompassPoint.swift
//  StructAndClass
//
//  Created by David Christian on 28/04/20.
//  Copyright © 2020 Apple Academy. All rights reserved.
//

import Foundation

enum CompassPoint{
    case north, south, east, west
    
    mutating func turnNorth(){
        self = .north
    }
    mutating func turnWest(){
        self = .west
    }
}
